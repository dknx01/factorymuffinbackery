<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 19.03.17 11:21
 */

namespace Dknx01\FactoryMuffinBackery\DVO;

class Muffin
{
    const CREATOR_METHOD_CREATE = 'create';
    const CREATOR_METHOD_INSTANCE = 'instance';
    const CREATOR_METHOD_SEED = 'seed';
    const AVAILABLE_CREATOR_METHODS = array(
        self::CREATOR_METHOD_CREATE,
        self::CREATOR_METHOD_INSTANCE,
        self::CREATOR_METHOD_SEED
    );

    /**
     * @var string
     */
    private $objectClass;

    /**
     * @var string
     */
    private $creatorMethod;

    /**
     * @var array
     */
    private $arguments = array();

    /**
     * @var int
     */
    private $seedTimes;

    /**
     * @var callable
     */
    private $maker;

    /**
     * @var callable
     */
    private $callback;

    /**
     * @return string
     */
    public function getObjectClass()
    {
        return $this->objectClass;
    }

    /**
     * @param string $objectClass
     *
     * @return Muffin
     */
    public function setObjectClass($objectClass)
    {
        $this->objectClass = $objectClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatorMethod()
    {
        return $this->creatorMethod;
    }

    /**
     * @param string $creatorMethod
     *
     * @return Muffin
     * @throws \InvalidArgumentException
     */
    public function setCreatorMethod($creatorMethod)
    {
        if (!in_array($creatorMethod, self::AVAILABLE_CREATOR_METHODS)) {
            throw new \InvalidArgumentException(
                'Creator method "' . $creatorMethod . '" is not allowed. Allowed are: ' .
                implode(', ', self::AVAILABLE_CREATOR_METHODS)
            );
        }
        $this->creatorMethod = $creatorMethod;
        return $this;
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     *
     * @return Muffin
     */
    public function setArguments(array $arguments)
    {
        $this->arguments = $arguments;
        return $this;
    }

    /**
     * @return int
     */
    public function getSeedTimes()
    {
        return $this->seedTimes;
    }

    /**
     * @param int $seedTimes
     *
     * @return Muffin
     */
    public function setSeedTimes($seedTimes)
    {
        $this->seedTimes = $seedTimes;
        return $this;
    }

    /**
     * @return callable
     */
    public function getMaker()
    {
        return $this->maker;
    }

    /**
     * @param callable $maker
     *
     * @return Muffin
     */
    public function setMaker($maker)
    {
        $this->maker = $maker;
        return $this;
    }

    /**
     * @return callable
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param callable $callback
     *
     * @return Muffin
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
        return $this;
    }
}
