<?php

namespace Dknx01\FactoryMuffinBundle\Factories;

/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.03.17 20:48
 */
interface FactoryMuffinAwareInterface
{
    /**
     * @param \League\FactoryMuffin\FactoryMuffin $factoryMuffin
     */
    public function setFactoryMuffin(\League\FactoryMuffin\FactoryMuffin $factoryMuffin);
}
