<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 19.03.17 11:44
 */

namespace Dknx01\FactoryMuffinBundle;

use Dknx01\FactoryMuffinBundle\DVO\Muffin;
use Dknx01\FactoryMuffinBundle\Factories\FactoryMuffinAwareInterface;
use League\FactoryMuffin\Exceptions\DefinitionAlreadyDefinedException;
use League\FactoryMuffin\Exceptions\DefinitionNotFoundException;
use League\FactoryMuffin\FactoryMuffin;

/**
 * @inheritdoc
 */
class MuffinBackery implements FactoryMuffinAwareInterface
{
    /**
     * @var Muffin[]
     */
    private $muffins = array();

    /**
     * @var FactoryMuffin
     */
    private $factoryMuffin;

    /**
     * @inheritdoc
     */
    public function setFactoryMuffin(\League\FactoryMuffin\FactoryMuffin $factoryMuffin)
    {
        $this->factoryMuffin = $factoryMuffin;
    }

    /**
     * add a muffin definition
     *
     * @param Muffin $muffin
     * @param string|null $alias
     * @return $this
     */
    public function addMuffin(Muffin $muffin, $alias = null)
    {
        if ($alias === null) {
            $alias = $muffin->getObjectClass();
        }

        $this->muffins[$alias] = $muffin;
        return $this;
    }

    /**
     * load all the defined object int the factory muffin instance
     *
     * @return $this
     */
    public function load()
    {
        /** @var Muffin $muffin */
        foreach ( $this->muffins as $muffin) {
            try {
                $this->factoryMuffin->define($muffin->getObjectClass());
            } catch (DefinitionAlreadyDefinedException $alreadyDefinedException) {
                continue;
            } catch (DefinitionNotFoundException $definitionNotFoundException) {
                if (strpos($muffin->getObjectClass(), ':') !== false) {
                    $name = $muffin->getObjectClass();
                    $group = current(explode(':', $name));
                    $class = str_replace($group . ':', '', $name);
                    $this->factoryMuffin->define($class);
                    unset($class, $group, $name);
                    $this->factoryMuffin->define($muffin->getObjectClass());
                }
            }
        }
        return $this;
    }

    /**
     * get an object by the class name
     *
     * @param string $objectClass
     * @return object|\object[]
     */
    public function get($objectClass)
    {
        return $this->getByAlias($objectClass);
    }

    /**
     * get an object by its alias
     *
     * @param string $alias
     * @return \object|\object[]
     * @throws \InvalidArgumentException
     */
    public function getByAlias($alias)
    {
        if (!array_key_exists($alias, $this->muffins)) {
            throw new \InvalidArgumentException('Alias ' . $alias . ' not defined');
        }

        $muffin = $this->muffins[$alias];

        if ($muffin->getCallback() !== null) {
            $this->factoryMuffin->getDefinition($muffin->getObjectClass())
                ->setCallback($muffin->getCallback());
        }
        if ($muffin->getMaker() !== null) {
            $this->factoryMuffin->getDefinition($muffin->getObjectClass())
                ->setMaker($muffin->getMaker());
        }

        if ($muffin->getCreatorMethod() === 'seed') {
            $object = $this->factoryMuffin
                ->seed($muffin->getSeedTimes(), $muffin->getObjectClass(), $muffin->getArguments());
        } else {
            $object = $this->factoryMuffin
                ->{$muffin->getCreatorMethod()}($muffin->getObjectClass(), $muffin->getArguments());
        }

        return $object;
    }

    /**
     * delete all definitions and reload them
     */
    public function resetAndReloadDefinitions()
    {
        $reflObj = new \ReflectionObject($this->factoryMuffin);
        $reflProperty = $reflObj->getProperty('definitions');
        $reflProperty->setAccessible(true);
        $reflProperty->setValue($this->factoryMuffin, array());
        $this->load();
    }
}
