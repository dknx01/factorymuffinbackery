**FactoryMuffinBackery**

A wrapper for Factory Muffin (https://github.com/thephpleague/factory-muffin)

#Usage

##The Muffin class

**Dknx01\FactoryMuffinBackery\DVO\Muffin:**

_setObjectClass:_ setting the full qualified class name

_setCreatorMethod:_ setting the create method: create, instance, seed

_setArguments:_ setting the arguments for the objects

_setSeedTimes:_ setting the number of object to be created

_setMaker:_ setting a maker function

_setCallback:_ setting a callback function 

###Example Class:
```php
namespace dknx01\FactoryMuffinBackery\Test;

class TestStub
{

}

use Dknx01\FactoryMuffinBundle\DVO\Muffin;
use Dknx01\FactoryMuffinBundle\MuffinBackery;
use League\FactoryMuffin\FactoryMuffin;
use League\FactoryMuffin\Stores\ModelStore;

class TestFactory
{
    /**
     * @var MuffinBackery
     */
    private $backery;

    public function createObject()
    {
        $this->backery = new MuffinBackery();
        $this->backery->setFactoryMuffin(new FactoryMuffin(new ModelStore()));

        $muffin = new Muffin();
        $muffin->setObjectClass(TestStub::class)->setCreatorMethod('instance');
        $this->backery->addMuffin($muffin, 'myStub');
    }
    /**
    * @return MuffinBackery
     */
    public function getBackery(): MuffinBackery
    {
        return $this->backery;
    }
}
```
### Test class
```php
class DefaultControllerTest extends TestCase
{
    public function testIndex()
    {
        $factory = new TestFactory();
        $factory->createObject();
        var_dump($factory->getBackery()->load()->getByAlias('myStub'));
    }
}
```