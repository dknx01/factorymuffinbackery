<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 25.03.17 17:31
 */

namespace Dknx01\FactoryMuffinBackery\Test;

use Dknx01\FactoryMuffinBundle\DVO\Muffin;
use Dknx01\FactoryMuffinBundle\MuffinBackery;
use League\FactoryMuffin\FactoryMuffin;
use League\FactoryMuffin\Stores\ModelStore;

class TestFactory
{
    /**
     * @var MuffinBackery
     */
    private $backery;

    public function createObject()
    {
        $this->backery = new MuffinBackery();
        $this->backery->setFactoryMuffin(new FactoryMuffin(new ModelStore()));

        $muffin = new Muffin();
        $muffin->setObjectClass(TestStub::class)->setCreatorMethod('instance');
        $this->backery->addMuffin($muffin, 'myStub');
    }
    /**
    * @return MuffinBackery
     */
    public function getBackery(): MuffinBackery
    {
        return $this->backery;
    }
}
